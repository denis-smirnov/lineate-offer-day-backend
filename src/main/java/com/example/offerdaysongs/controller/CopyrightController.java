package com.example.offerdaysongs.controller;


import com.example.offerdaysongs.dto.CompanyDto;
import com.example.offerdaysongs.dto.CopyrightDto;
import com.example.offerdaysongs.dto.RecordingDto;
import com.example.offerdaysongs.dto.SingerDto;
import com.example.offerdaysongs.dto.requests.CreateCopyrightRequest;
import com.example.offerdaysongs.model.Copyright;
import com.example.offerdaysongs.service.CopyrightService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/copyrights")
public class CopyrightController {
    private static final String ID = "id";
    private static final String OWNER = "owner";
    private static final String START = "start";
    private static final String END = "end";

    private final CopyrightService copyrightService;

    public CopyrightController(CopyrightService copyrightService) {
        this.copyrightService = copyrightService;
    }

    @GetMapping("/")
    public List<CopyrightDto> getAll(){
        return copyrightService.getAll().stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id:[\\d]+}")
    public CopyrightDto get(@PathVariable(ID) long id) {
        var copyright = copyrightService.getById(id);
        return convertToDto(copyright);
    }

    @GetMapping("")
    public List<CopyrightDto> getByOwner(@RequestParam(OWNER) long companyId) {
        return copyrightService.getByOwner(companyId).stream()
            .map(this::convertToDto).collect(Collectors.toList());
    }

    @GetMapping("/period")
    public List<CopyrightDto> getByPeriod(@RequestParam(START) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) ZonedDateTime periodStart,
                                          @RequestParam(END)   @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) ZonedDateTime periodEnd) {
        return copyrightService.getByPeriod(periodStart, periodEnd).stream()
            .map(this::convertToDto).collect(Collectors.toList());
    }

    @PostMapping("/")
    public CopyrightDto create(@RequestBody CreateCopyrightRequest request) {
        return convertToDto(copyrightService.create(request));
    }

    private CopyrightDto convertToDto(Copyright copyright) {
        CompanyDto ownerDto = Optional.ofNullable(copyright.getOwner())
            .map(company -> new CompanyDto(company.getId(), company.getName()))
            .orElse(null);
        RecordingDto recordingDto = Optional.ofNullable(copyright.getRecording())
            .map(recording -> new RecordingDto(recording.getId(),
                                               recording.getTitle(),
                                               recording.getVersion(),
                                               recording.getReleaseTime(),
                                               Optional.ofNullable(recording.getSinger())
                                                   .map(singer -> new SingerDto(singer.getId(), singer.getName()))
                                                   .orElse(null)))
            .orElse(null);
        return new CopyrightDto(copyright.getId(),
                                copyright.getValidFrom(),
                                copyright.getValidTo(),
                                copyright.getUsagePrice(),
                                recordingDto,
                                ownerDto);
    }
}