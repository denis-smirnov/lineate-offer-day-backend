package com.example.offerdaysongs.model;


import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@Entity
public class Copyright {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private ZonedDateTime validFrom;
    private ZonedDateTime validTo;

    private BigDecimal usagePrice;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "recording_id", referencedColumnName = "id")
    Recording recording;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    Company owner;

}
