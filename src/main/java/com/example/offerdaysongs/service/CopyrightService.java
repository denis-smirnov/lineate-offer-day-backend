package com.example.offerdaysongs.service;

import com.example.offerdaysongs.dto.requests.CreateCopyrightRequest;
import com.example.offerdaysongs.model.Company;
import com.example.offerdaysongs.model.Copyright;
import com.example.offerdaysongs.model.Recording;
import com.example.offerdaysongs.model.Singer;
import com.example.offerdaysongs.repository.CompanyRepository;
import com.example.offerdaysongs.repository.CopyrightRepository;
import com.example.offerdaysongs.repository.RecordingRepository;
import com.example.offerdaysongs.repository.SingerRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class CopyrightService {
    private final CopyrightRepository copyrightRepository;
    private final CompanyRepository companyRepository;
    private final RecordingRepository recordingRepository;
    private final SingerRepository singerRepository;

    public CopyrightService(CopyrightRepository copyrightRepository, CompanyRepository companyRepository,
                            RecordingRepository recordingRepository, SingerRepository singerRepository) {
        this.copyrightRepository = copyrightRepository;
        this.recordingRepository = recordingRepository;
        this.companyRepository = companyRepository;
        this.singerRepository = singerRepository;
    }

    public List<Copyright> getAll() {
        return copyrightRepository.findAll();
    }

    public Copyright getById(long id) {
        return copyrightRepository.getById(id);
    }

    public List<Copyright> getByOwner(long companyId) {
        Company owner = companyRepository.getById(companyId);
        return copyrightRepository.findByOwner(owner);
    }
    
    public List<Copyright> getByPeriod(ZonedDateTime periodStart, ZonedDateTime periodEnd) {
        return copyrightRepository.findByPeriod(periodStart, periodEnd);
    }

    @Transactional
    public Copyright create(CreateCopyrightRequest request) {
        Copyright copyright = new Copyright();
        copyright.setValidFrom(request.getValidFrom());
        copyright.setValidTo(request.getValidTo());
        copyright.setUsagePrice(request.getUsagePrice());

        Optional.ofNullable(request.getRecording())
            .ifPresent(recordingDto -> {
                Recording copyrightRecording = recordingRepository
                    .findById(recordingDto.getId()).orElseGet(() -> {
                        Recording recording = new Recording();
                        recording.setTitle(recordingDto.getTitle());
                        recording.setVersion(recordingDto.getVersion());
                        recording.setReleaseTime(recordingDto.getReleaseTime());
                        Optional.ofNullable(recordingDto.getSinger())
                            .ifPresent(singerDto -> {
                                Singer singer = singerRepository.findById(singerDto.getId())
                                    .orElseGet(() -> {
                                        Singer newSinger = new Singer();
                                        newSinger.setName(singerDto.getName());
                                        return singerRepository.save(newSinger);
                                    });
                                recording.setSinger(singer);
                            });
                        return recording;
                    });
                copyright.setRecording(copyrightRecording);
            });

        Optional.ofNullable(request.getOwner())
            .ifPresent(companyDto -> {
                Company company = companyRepository.findById(companyDto.getId())
                    .orElseGet(() -> {
                        Company newCompany = new Company();
                        newCompany.setName(companyDto.getName());
                        return companyRepository.save(newCompany);
                    });
                copyright.setOwner(company);
            });
        return copyrightRepository.save(copyright);
    }
}
