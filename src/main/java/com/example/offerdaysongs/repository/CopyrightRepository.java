package com.example.offerdaysongs.repository;

import com.example.offerdaysongs.model.Company;
import com.example.offerdaysongs.model.Copyright;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.time.ZonedDateTime;
import java.util.List;

public interface CopyrightRepository extends JpaRepository<Copyright, Long>, JpaSpecificationExecutor<Copyright> {

    List<Copyright> findByOwner(Company owner);

    @Query(value = "SELECT * FROM Copyright WHERE (?1 <= valid_from AND valid_from <= ?2) " +
            "or (?1 <= valid_to AND valid_to <= ?2)", nativeQuery = true)
    List<Copyright> findByPeriod(ZonedDateTime periodStart, ZonedDateTime periodEnd);

}
