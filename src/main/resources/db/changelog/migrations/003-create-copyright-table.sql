CREATE TABLE copyright
(
    id              BIGSERIAL PRIMARY KEY,
    valid_from      TIMESTAMP,
    valid_to        TIMESTAMP,
    usage_price     DECIMAL,

    recording_id    BIGINT REFERENCES recording (id),
    owner_id        BIGINT REFERENCES company (id)
);